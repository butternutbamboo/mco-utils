# mco-utils

This repository includes a collection of utilities for the Minecraft server [Minecraft Online](minecraftonline.com) (MCO).

## Utilities

- [ingame/book-generator.py](ingame/book-generator.py): Split any plain text into valid Minecraft book pages, respecting limits for line width and page length.
- [wiki/birthdays.py](wiki/birthdays.py): Scrape and parse birthday information from MCO wiki `User:` pages.
- [wiki/penguindungeons.py](wiki/penguindungeons.py): Scrape and parse loot tables from the [PenguinDungeons repository](https://gitlab.com/minecraftonline/PenguinDungeons/-/tree/master/src/main/resources/assets/penguindungeons/loottable/penguindungeons) and generate MediaWiki code to represent those tables as a single MediaWiki table.