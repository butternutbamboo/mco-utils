#!/usr/bin/env python3

import pathlib
import re
import sys

MAX_LINES_PER_PAGE = 14

# MAX_LINE_WIDTH = 116 # 115 pixels + 1 blank
MAX_LINE_WIDTH = 114  # some extra room for safety

CHARACTER_REPLACEMENTS = {
    '’': "'", '“': '\'', '”': '\'', '—': '-'
}

# https://github.com/TheWilley/text2book/blob/master/src/main.ts#L46C1-L291C35
CHARACTER_WIDTHS = {
    'À': 6, 'Á': 6, 'Â': 6, 'È': 6, 'Ê': 6, 'Ë': 6, 'Í': 4, 'Ó': 6, 'Ô': 6,
    'Õ': 6, 'Ú': 6, 'ß': 6, 'ã': 6, 'õ': 6, 'ğ': 6, 'İ': 4, 'ı': 4, 'Œ': 6,
    'œ': 7, 'Ş': 6, 'ş': 6, 'Ŵ': 6, 'ŵ': 6, 'ž': 6, 'ȇ': 6, '!': 2, '\'': 5,
    '#': 6, '$': 6, '%': 6, '&': 6, "'": 3, '(': 5, ')': 5, '*': 5, '+': 6,
    ',': 2, '-': 6, '.': 2, '/': 6, '0': 6, '1': 6, '2': 6, '3': 6, '4': 6,
    '5': 6, '6': 6, '7': 6, '8': 6, '9': 6, ':': 2, ';': 2, '<': 5, '=': 6,
    '>': 5, '?': 6, '@': 7, 'A': 6, 'B': 6, 'C': 6, 'D': 6, 'E': 6, 'F': 6,
    'G': 6, 'H': 6, 'I': 4, 'J': 6, 'K': 6, 'L': 6, 'M': 6, 'N': 6, 'O': 6,
    'P': 6, 'Q': 6, 'R': 6, 'S': 6, 'T': 6, 'U': 6, 'V': 6, 'W': 6, 'X': 6,
    'Y': 6, 'Z': 6, '[': 4, ']': 4, '^': 6, '_': 6, '`': 3, 'a': 6, 'b': 6,
    'c': 6, 'd': 6, 'e': 6, 'f': 5, 'g': 6, 'h': 6, 'i': 2, 'j': 6, 'k': 5,
    'l': 3, 'm': 6, 'n': 6, 'o': 6, 'p': 6, 'q': 6, 'r': 6, 's': 6, 't': 4,
    'u': 6, 'v': 6, 'w': 6, 'x': 6, 'y': 6, 'z': 6, '{': 5, '|': 2, '}': 5,
    '~': 7, 'Ç': 6, 'ü': 6, 'é': 6, 'â': 6, 'ä': 6, 'à': 6, 'å': 6, 'ç': 6,
    'ê': 6, 'ë': 6, 'è': 6, 'ï': 4, 'î': 6, 'ì': 3, 'Ä': 6, 'Å': 6, 'É': 6,
    'æ': 6, 'Æ': 6, 'ô': 6, 'ö': 6, 'ò': 6, 'û': 6, 'ù': 6, 'ÿ': 6, 'Ö': 6,
    'Ü': 6, 'ø': 6, '£': 6, 'Ø': 6, '×': 4, 'ƒ': 6, 'á': 6, 'í': 3, 'ó': 6,
    'ú': 6, 'ñ': 6, 'Ñ': 6, 'ª': 6, 'º': 6, '¿': 6, '®': 7, '¬': 6, '½': 6,
    '¼': 6, '¡': 2, '«': 6, '»': 6, '░': 8, '▒': 9, '▓': 9, '│': 6, '┤': 6,
    '╡': 6, '╢': 8, '╖': 8, '╕': 6, '╣': 8, '║': 8, '╗': 8, '╝': 8, '╜': 8,
    '╛': 6, '┐': 6, '└': 9, '┴': 9, '┬': 9, '├': 9, '─': 9, '┼': 9, '╞': 9,
    '╟': 9, '╚': 9, '╔': 9, '╩': 9, '╦': 9, '╠': 9, '═': 9, '╬': 9, '╧': 9,
    '╨': 9, '╤': 9, '╥': 9, '╙': 9, '╘': 9, '╒': 9, '╓': 9, '╫': 9, '╪': 9,
    '┘': 6, '┌': 9, '█': 9, '▄': 9, '▌': 5, '▐': 9, '▀': 9, 'α': 8, 'β': 7,
    'Γ': 7, 'π': 8, 'Σ': 7, 'σ': 8, 'μ': 8, 'τ': 8, 'Φ': 7, 'Θ': 8, 'Ω': 8,
    'δ': 7, '∞': 9, '∅': 9, '∈': 6, '∩': 7, '≡': 7, '±': 7, '≥': 7, '≤': 7,
    '⌠': 9, '⌡': 6, '÷': 7, '≈': 8, '°': 7, '∙': 6, '·': 6, '√': 9, 'ⁿ': 7,
    '²': 6, '■': 7, ' ': 4
}


def usage():
    print(f'usage: {sys.argv[0]} /path/to/input.txt [/path/to/output.txt]')


# https://stackoverflow.com/a/312464/21894717
def chunk(items: list, size: int):
    for i in range(0, len(items), size):
        yield items[i:i + size]


def wrap_line(line: str,
              character_widths: dict[str, int] = CHARACTER_WIDTHS,
              max_line_width: int = MAX_LINE_WIDTH) -> list[str]:
    lines = []

    line = line.rstrip()

    words = re.split(r'(\s+)', line)
    this_line = ''
    this_line_width = 0

    for word in words:
        if word == '':
            continue

        word_width = sum(character_widths[character] for character in word)
        new_line_width = this_line_width + word_width

        if new_line_width > max_line_width:
            lines.append(this_line.rstrip())
            this_line = word
            this_line_width = word_width
            continue

        this_line += word
        this_line_width = new_line_width

    lines.append(this_line)

    return lines


def main():
    text = None

    try:
        with pathlib.Path(sys.argv[1]).open('r') as input_fp:
            text = input_fp.read()
    except (IndexError, FileNotFoundError):
        usage()
        sys.exit()

    for character, replacement in CHARACTER_REPLACEMENTS.items():
        text = text.replace(character, replacement)

    lines = [
        line_wrapped
        for line_raw in text.split('\n')
        for line_wrapped in wrap_line(line_raw)
    ]
    pages = list(chunk(lines, MAX_LINES_PER_PAGE - 1))

    if pages[-1] == ['']:
        pages = pages[:-1]

    try:
        if sys.argv[2] == sys.argv[1]:
            print('input and output cannot be the same file')
            sys.exit()
        output_fp = pathlib.Path(sys.argv[2]).open('w')
    except IndexError:
        output_fp = sys.stdout
        print('defaulting to stdout')

    n_pages = len(pages)
    for i, page in enumerate(pages):
        output_fp.write(f'══════════ Page {i + 1} of {n_pages} ══\n')
        output_fp.write('\n'.join(page))
        output_fp.write('\n')

    if output_fp is not sys.stdout:
        output_fp.close()
        print(f'wrote to {sys.argv[2]}')


if __name__ == '__main__':
    main()
