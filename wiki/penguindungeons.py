#!/usr/bin/env python
# List all used functions (run from loot table directory):
#
#   cat * | jq -r .pools[].entries[].functions[].function 2> /dev/null | sort | uniq -c
#
# List all used conditions (run from loot table directory):
#
#   cat * | jq -r .pools[].entries[].conditions[].condition 2> /dev/null | sort | uniq -c
#
# Links:
#
# - PenguinDungeons loot tables:
#   https://gitlab.com/minecraftonline/PenguinDungeons/-/tree/master/src/main/resources/assets/penguindungeons/loottable/penguindungeons
# - abamacus' code for parsing PenguinDungeons (thanks abamacus!):
#   https://gitlab.com/codesine/PenguinDungeons/-/blob/master/src/main/resources/assets/penguindungeons/loottable/penguindungeons/LootTable.html
# - Loot table docs (from the official Minecraft documentation):
#   https://learn.microsoft.com/en-us/minecraft/creator/documents/loottableconditions
#   https://learn.microsoft.com/en-us/minecraft/creator/documents/lootandtradetablefunctions
# - Loot table docs (from the Minecraft wiki):
#   https://minecraft.fandom.com/wiki/Loot_table
# - Predicate docs (from the Minecraft wiki):
#   https://minecraft.fandom.com/wiki/Predicate
# - Loot table probability calculator (thanks Anna!):
#   https://skylinerw.com/evaluator/
# - Minecraft forum post (thanks Anna!):
#   https://www.minecraftforum.net/forums/minecraft-java-edition/redstone-discussion-and/commands-command-blocks-and/2546347-1-12-custom-loot-tables

import base64
import dataclasses
import json
import re
import typing

import gitlab
import nbtlib
import requests

PENGUINDUNGEONS_PROJECT_ID = 26865309

# Path within the PenguinDungeons repo pointing to the directory of loot tables
LOOT_TABLES_PATH = 'src/main/resources/assets/penguindungeons/loottable/penguindungeons'

# For future use: this file is a directory for the PrismarineJS minecraft-data
# JSON files: https://github.com/PrismarineJS/minecraft-data/blob/master/data/dataPaths.json
PRISMARINEJS_ITEMS_URL = 'https://raw.githubusercontent.com/PrismarineJS/minecraft-data/master/data/pc/1.12/items.json'
PRISMARINEJS_ENTITIES_URL = 'https://github.com/PrismarineJS/minecraft-data/raw/master/data/pc/1.12/entities.json'

PENGUINDUNGEONS_ENTITY_NAMES_URL = 'https://gitlab.com/minecraftonline/PenguinDungeons/-/raw/master/src/main/java/com/minecraftonline/penguindungeons/customentity/CustomEntityTypes.java'

# Looting level to use for looting_multiplier calculations; see:
# https://minecraft.fandom.com/wiki/Predicate
LOOTING_LEVEL = 3

# Luck level to use as a multiplier for bonus_luck when the lucky paw is worn;
# see: https://minecraft.fandom.com/wiki/Loot_table#Pool
LUCK_LEVEL_LUCKY_PAW = 1

SIG_FIGS = 2


class CustomMobLookup:
    """ Lookup table from JSON loot tables to PenguinDungeon names """

    # First group is for the item name; second group is for the JSON name
    REGEX = 'public static final [^\s]+ ([^\s]+).*?"([^"]+)'

    def __init__(self, entities_url: str = PENGUINDUNGEONS_ENTITY_NAMES_URL):
        print(f'Retrieving CustomEntityTypes.java from {entities_url}')
        response = requests.get(entities_url)
        response.raise_for_status()
        self.entity_names_dict = dict()
        for result in re.finditer(self.REGEX, response.content.decode()):
            entity_name, entity_id = result.groups()
            self.entity_names_dict[entity_id] = entity_name.replace('_', ' ').title()

    def __getitem__(self, item: str) -> typing.Optional[str]:
        return self.entity_names_dict.get(item.replace('.json', ''))


class PrismarineJSItemEntityLookup:
    """ Lookup table from minecraft item / entity IDs to display names, powered
     by PrismarineJS data """

    def __init__(self, url: str, id_prefix: str = ""):
        self.id_prefix = id_prefix
        print(f'Retrieving {url.split("/")[-1]} from {url}')
        response = requests.get(url)
        response.raise_for_status()
        self.items = response.json()
        self.item_names_dict = {
            item['name']: item['displayName']
            for item in self.items
        }

    def __getitem__(self, item: str) -> typing.Optional[str]:
        if not item.startswith(self.id_prefix):
            return
        return self.item_names_dict.get(item.replace(self.id_prefix, ''))


@dataclasses.dataclass
class LootTableItem:
    name: str
    min_count: int
    max_count: int
    min_rolls: int
    max_rolls: int
    bonus_rolls: float
    probability: float
    min_count_looting: int
    max_count_looting: int
    probability_looting: float
    nbt_name: typing.Optional[str] = None


# https://stackoverflow.com/a/48812729
def sig_figs(x: float, n: int) -> str:
    return '{:g}'.format(float('{:.{p}g}'.format(x, p=n)))


def combine_counts(min_count: int, max_count: int) -> str:
    return str(max_count) if min_count == max_count else f'{min_count}-{max_count}'


def stream_loot_table(loot_table_data: dict
                      ) -> typing.Generator[LootTableItem, None, None]:
    """ Parse a loot table JSON and stream LootTableItem objects from it """

    for pool in loot_table_data.get('pools', []):
        entries = pool['entries']
        pool_weight = sum(item.get('weight', 1) for item in entries)
        for item in entries:
            if item['type'] == 'empty':
                continue

            # Base values
            min_count = 1
            max_count = 1
            probability = item.get('weight', 1) / pool_weight  # Probability per roll

            # To be set by conditions and functions
            bonus_rolls = 0
            min_count_looting = min_count
            max_count_looting = max_count
            looting_multiplier = 0
            nbt_name = None

            # Rolls
            rolls = pool['rolls']
            if type(rolls) is dict:
                min_rolls = rolls['min']
                max_rolls = rolls['max']
            else:
                min_rolls = rolls
                max_rolls = rolls
            bonus_rolls = round(pool.get('bonus_rolls', 0) * LUCK_LEVEL_LUCKY_PAW)

            # Looting table conditions (primarily probability modifiers)
            # https://learn.microsoft.com/en-us/minecraft/creator/documents/loottableconditions
            for condition in item.get('conditions', []):
                if condition['condition'] == 'random_chance':
                    probability *= condition['chance']
                if condition['condition'] == 'random_chance_with_looting':
                    probability *= condition['chance']
                    looting_multiplier = condition['looting_multiplier']

            # Looting table functions (primarily count modifiers)
            # https://learn.microsoft.com/en-us/minecraft/creator/documents/lootandtradetablefunctions
            for function_ in item.get('functions', []):
                if function_['function'] == 'set_count':
                    count = function_['count']
                    if type(count) is dict:
                        min_count = count['min']
                        max_count = count['max']
                    else:
                        min_count = count
                        max_count = count
                if function_['function'] == 'looting_enchant':
                    min_count_looting = function_['count']['min']
                    max_count_looting = function_['count']['max']
                elif function_['function'] == 'set_nbt':
                    nbt = dict(nbtlib.parse_nbt(function_['tag']))

                    # First try: player head
                    nbt_name = nbt.get('SkullOwner', dict()).get('Name')

                    # Second try: item with lore
                    if not nbt_name:
                        nbt_name = nbt.get('display', dict()).get('Name')

                    # If successful: strip any color codes
                    if nbt_name:
                        nbt_name = re.sub('§.', '', nbt_name)

            # Looting calculations (https://minecraft.fandom.com/wiki/Predicate)
            probability_looting = probability + looting_multiplier * LOOTING_LEVEL

            yield LootTableItem(
                name=item['name'],
                min_count=min_count,
                max_count=max_count,
                min_rolls=min_rolls,
                max_rolls=max_rolls,
                bonus_rolls=bonus_rolls,
                probability=probability,
                min_count_looting=min_count_looting,
                max_count_looting=max_count_looting,
                probability_looting=probability_looting,
                nbt_name=nbt_name
            )


def main(as_wiki_table: bool = True):
    # Initialize objects
    gl = gitlab.Gitlab()
    custom_mob_lookup = CustomMobLookup()
    item_lookup = PrismarineJSItemEntityLookup(PRISMARINEJS_ITEMS_URL, 'minecraft:')
    entity_lookup = PrismarineJSItemEntityLookup(PRISMARINEJS_ENTITIES_URL, 'minecraft:entities/')

    # Query GitLab API for loot tables
    project = gl.projects.get(PENGUINDUNGEONS_PROJECT_ID)
    loot_tables = project.repository_tree(path=LOOT_TABLES_PATH, get_all=True)

    if as_wiki_table:
        print('''
{| class="wikitable sortable"
|+ Custom Mobs
!Mob!!Item!!Count!!Probability!!Rolls''')

    for loot_table in loot_tables:
        # This is the content of a JSON loot table
        loot_table_data = json.loads(base64.b64decode(
            project.repository_blob(loot_table['id'])['content']
        ))

        mob_name = custom_mob_lookup[loot_table['name']]

        # If we can't parse the mob name from the CustomEntityTypes.java, then just work
        # with the name of the file
        if not mob_name:
            mob_name = loot_table['name'].replace('.json', '').replace('_', '').title()

        for item in stream_loot_table(loot_table_data):
            name = item.nbt_name or item_lookup[item.name] or entity_lookup[item.name] or item.name
            # item_name = item_lookup[item.name] or entity_lookup[item.name] or item.name
            # display_name = item.nbt_name or item_name
            # name = display_name + (f' ({item_name})' if item_name != display_name else '')

            percent = sig_figs(item.probability, SIG_FIGS)
            percent_looting = sig_figs(item.probability_looting, SIG_FIGS)

            count = combine_counts(item.min_count, item.max_count)
            count_looting = combine_counts(item.min_count_looting, item.max_count_looting)

            rolls = combine_counts(item.min_rolls, item.max_rolls)
            rolls_lucky = combine_counts(
                round(item.min_rolls + item.bonus_rolls),
                round(item.max_rolls + item.bonus_rolls)
            )

            if as_wiki_table:
                print('|-')
                print('| ' + ' || | '.join([
                    mob_name,
                    name,
                    count + (f' ({count_looting})' if count != count_looting else ''),
                    str(percent) + (f' ({percent_looting})' if percent != percent_looting else ''),
                    str(rolls) + (f' ({rolls_lucky})' if rolls != rolls_lucky else '')
                ]).lstrip(' '))
            else:
                plural_count = 's' if item.max_count != 1 else ''
                plural_rolls = 's' if item.max_rolls != 1 else ''
                print(f'[{mob_name}] {name}: '
                      f'{percent}% for {count} item{plural_count}; '
                      f'{percent_looting}% for {count_looting} item{plural_count} with looting {LOOTING_LEVEL}; '
                      f'{rolls} roll{plural_rolls} ({rolls_lucky} if with lucky paw)')

    if as_wiki_table:
        print('|}')


if __name__ == '__main__':
    main(as_wiki_table=False)
